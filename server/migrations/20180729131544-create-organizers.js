'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('organizers', {

            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },
            NAME: {
                type: Sequelize.STRING,
                allowNull: false
            },
            PICK_UP_POINT: {
                type: Sequelize.STRING
            },
            DESTINATION: {
                type: Sequelize.STRING
            },
            CONTACT_NO: {
                type: Sequelize.STRING
            },
            EMAIL: {
                type: Sequelize.STRING
            },
            NO_OF_PASSENGERS: {
                type: Sequelize.STRING
            },
            PRICE_PER_SEAT: {
                type: Sequelize.STRING
            },
            RYDE_VEHICLE: {
                type: Sequelize.BOOLEAN,
                allowNull: false,
                defaultValue: false
            },
            NO_OF_VEHICLES: {
                type: Sequelize.STRING,
                allowNull: false,
                defaultValue: 0
            },
            VEHICLE_TYPE: {
                type: Sequelize.STRING,
                allowNull: false,
                defaultValue: 'NONE'
            },
            USER: {
                type: Sequelize.STRING,
                allowNull: false,
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
        });
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('events');
    }
};