'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('users', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },
            FULL_NAME: {
                type: Sequelize.STRING
            }, EMAIL: {
                type: Sequelize.STRING
            }, PHONE_NUMBER: {
                type: Sequelize.STRING
            },
            PASSWORD: {
                type: Sequelize.STRING
            },
            UUID: {
                type: Sequelize.STRING,
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
        });
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('users');
    }
};