'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('rentals', {

            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },
            NAME: {
                type: Sequelize.STRING
            },
            CONTACT: {
                type: Sequelize.STRING
            },
            EMAIL: {
                type: Sequelize.STRING
            },
            SERVICE_TYPE: {
                type: Sequelize.STRING
            },
            VEHICLE_TYPE: {
                type: Sequelize.STRING
            },
            PICK_UP_POINT: {
                type: Sequelize.STRING
            },
            DESTINATION: {
                type: Sequelize.STRING
            },
            START: {
                type: Sequelize.DATEONLY,
                allowNull: false
            },
            END: {
                type: Sequelize.DATEONLY,
                allowNull: false
            },
            UUID: {
                type: Sequelize.STRING
            },
            USER: {
                type: Sequelize.STRING,
                allowNull: false
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
        });
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('rentals');
    }
};