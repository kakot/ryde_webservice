'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('trips', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },
            ROUTE: {
                type: Sequelize.STRING
            },
            WEEKLY_PRICE: {
                type: Sequelize.STRING
            },
            MONTHLY_PRICE: {
                type: Sequelize.STRING
            },
            MORNING: {
                type: Sequelize.STRING
            },
            EVENING: {
                type: Sequelize.STRING
            },
            ZONE: {
                type: Sequelize.INTEGER,
                onDelete: 'CASCADE',
                references: {
                    model: 'zones',
                    key: 'id',
                    as: 'zone',
                },
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
        });
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('trips');
    }
};