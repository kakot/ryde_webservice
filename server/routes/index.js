const authController = require('../controllers').authController;
const userController = require('../controllers').userController;
const paymentController = require('../controllers').paymentController;
const tripsController = require('../controllers').tripsController;
const rentalController = require('../controllers').rentalController;
const organizerController = require('../controllers').organizerController;
const zoneController = require('../controllers').zoneController;
 const verify = require('../controllers/authcontroller').verify;

module.exports = (app) => {
    // auth route
    app.post('/login', authController.authenticate);
    app.post('/createUser', userController.createUser);

    // trips
    app.get('/allTrips', verify, tripsController.allTrips);
    app.get('/userTrips', verify, tripsController.userTrips);
    app.post('/createTrip', verify, tripsController.createTrip);

    // rental
    app.get('/allRentals', verify, rentalController.allRentals);
    app.get('/userRentals', verify, rentalController.userRentals);
    app.post('/rent', verify, rentalController.rent);

    // organizer
    app.get('/userOrganized', verify, organizerController.userOrganized);
    app.post('/organize', verify, organizerController.organize);


    // payments
    app.post('/pay', verify, paymentController.pay);
    app.post('/confirmPayment', paymentController.confirmPayment);
    app.get('/allPayments', verify, paymentController.allPayments);
    app.get('/userPayments', verify, paymentController.userPayments);


    // Zones
    app.post('/zone', verify, zoneController.zone);
};
