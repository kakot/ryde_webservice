'use strict';

module.exports = (sequelize, DataTypes) => {
    const user_trip = sequelize.define('user_trip', {
        USER: DataTypes.INTEGER,
        TRIP: DataTypes.INTEGER
    }, {});



    return user_trip;
};