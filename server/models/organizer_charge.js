//  charges (1 - 7 days (GHS80); 7 - 14 days (GHS150);
// 14 - 21 days (GHS200); 1 month plus (GHS300))

module.exports = (sequelize, DataTypes) => {
    const organizer_charge = sequelize.define('organizer_charge', {
        NO_OF_DAYS: DataTypes.STRING,
        CHARGE: DataTypes.STRING
    }, {});

    return organizer_charge;
};