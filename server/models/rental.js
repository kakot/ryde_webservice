'use strict';
const btoa = require('btoa');

module.exports = (sequelize, DataTypes) => {
    const rental = sequelize.define('rental', {
        NAME: DataTypes.STRING,
        CONTACT: DataTypes.STRING,
        EMAIL: DataTypes.STRING,
        SERVICE_TYPE: DataTypes.STRING,
        VEHICLE_TYPE: DataTypes.STRING,
        PICK_UP_POINT: DataTypes.STRING,
        DESTINATION: DataTypes.STRING,
        START: DataTypes.DATEONLY,
        END: DataTypes.DATEONLY,
        UUID: DataTypes.STRING,
        USER: DataTypes.STRING
    }, {
        hooks: {
            beforeCreate: (rental, options) => {
                rental.UUID = btoa(rental.CONTACT + rental.EMAIL + (Math.random() * 100));
            }
        }
    });
    rental.associate = function(models) {
    // associations can be defined here
  };
  return rental;
};