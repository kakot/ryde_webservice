'use strict';
module.exports = (sequelize, DataTypes) => {
    const payment = sequelize.define('payment', {
        transactionId: DataTypes.STRING,
        externalTransactionId: DataTypes.STRING,
        clientReference: DataTypes.STRING,
        description: DataTypes.STRING,
        amount: DataTypes.STRING
    }, {});
    payment.associate = function(models) {
    // associations can be defined here
  };
  return payment;
};