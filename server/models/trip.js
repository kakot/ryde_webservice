'use strict';
module.exports = (sequelize, DataTypes) => {
    const trip = sequelize.define('trip', {
        ROUTE: DataTypes.STRING,
        WEEKLY_PRICE: DataTypes.STRING,
        MONTHLY_PRICE: DataTypes.STRING,
        MORNING: DataTypes.STRING,
        EVENING: DataTypes.STRING,
        ZONE: DataTypes.INTEGER
    }, {});

    trip.associate = function(models) {
        // associations can be defined here
        trip.belongsTo(models.zone, {
            foreignKey: 'ZONE',
            onDelete: 'CASCADE'
        });

        trip.belongsToMany(models.user, {
            through: 'user_trips',
            as: 'users',
            foreignKey: 'TRIP'
        });
    };
    return trip;
};