'use strict';

const hash = require('password-hash');
const btoa = require('btoa');

module.exports = (sequelize, DataTypes) => {
    const user = sequelize.define('user', {
        FULL_NAME: DataTypes.STRING,
        EMAIL: DataTypes.STRING,
        PHONE_NUMBER: DataTypes.STRING,
        PASSWORD: DataTypes.STRING,
        UUID: DataTypes.STRING
    }, {
        hooks: {
            beforeCreate: (user, options) => {
                user.PASSWORD = hash.generate(user.PASSWORD);
                user.UUID = btoa(user.PHONE_NUMBER + user.EMAIL);
            }
        }
    });
    user.associate = function(models) {
    // associations can be defined here
        user.belongsToMany(models.trip, {
            through: 'user_trips',
            as: 'trips',
            foreignKey: 'USER'
        });
  };
  return user;
};