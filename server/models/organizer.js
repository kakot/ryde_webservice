'use strict';
module.exports = (sequelize, DataTypes) => {
    const organizer = sequelize.define('organizer', {
        NAME: DataTypes.STRING,
        PICK_UP_POINT: DataTypes.STRING,
        DESTINATION: DataTypes.STRING,
        CONTACT_NO: DataTypes.STRING,
        EMAIL: DataTypes.STRING,
        NO_OF_PASSENGERS: DataTypes.STRING,
        PRICE_PER_SEAT: DataTypes.STRING,
        RYDE_VEHICLE: DataTypes.BOOLEAN,
        NO_OF_VEHICLES: DataTypes.STRING,
        VEHICLE_TYPE: DataTypes.STRING,
        USER: DataTypes.STRING
    }, {});
  return organizer;
};
