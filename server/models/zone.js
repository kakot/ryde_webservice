
'use strict';
module.exports = (sequelize, DataTypes) => {
    const zone = sequelize.define('zone', {
        NAME: DataTypes.STRING,
    }, {});
    zone.associate = function(models) {
        // associations can be defined here
        zone.hasMany(models.trip, {
            foreignKey: 'ZONE',
            as: 'trips',
        });
    };
    return zone;
};
