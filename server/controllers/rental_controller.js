const rentalsModel = require('../models').rental;
const nodemailer = require('nodemailer');

module.exports = {
    allRentals: (req, res) => {
        return rentalsModel.findAll()
            .then(rentals => res.send(rentals))
            .catch(error => res.json({
                status: 'failed',
                message: error
            }));
    },
    userRentals: (req, res) => {
        return  rentalsModel.findAll({
            where: {
                USER: req.body.uuid
            }
        })
            .then(rentals => res.send(rentals))
            .catch(error => res.json({
                status: 'failed',
                message: error
            }));
    },
    rent: (req, res) => {
        return rentalsModel
            .create({
                NAME: req.body.name,
                CONTACT: req.body.phone,
                EMAIL: req.body.email,
                SERVICE_TYPE: req.body.service,
                VEHICLE_TYPE: req.body.vehicle,
                PICK_UP_POINT: req.body.pick_up_point,
                DESTINATION: req.body.destination,
                START: req.body.start,
                END: req.body.end,
                UUID: '',
                USER: req.body.uuid,
            })
            .then(rental => {
                // send feedback here
               // res.send(rental);

                const transporter = nodemailer.createTransport({
                    host: 'smtp.zoho.com',
                    port: 465,
                    secure: true,
                    auth: {
                        user: 'devwilfred@zoho.com',
                        pass: 'UwDG8P7sceGt'
                    },
                    tls: {
                        rejectUnauthorized: false
                    }
                });

                const mailOptions = {
                    from: 'devwilfred@zoho.com', // sender address
                    to: req.body.email, // list of receivers
                    subject: 'feedback', // Subject line
                    html: '<p>head to .... to make payment</p>'// plain text body
                };

                transporter.sendMail(mailOptions, function (err, info) {
                    if(err)
                        res.json({
                            status: 'failed',
                            message: err
                        });
                    else
                        res.json({
                            status: 'success',
                            message: 'An email has been sent to ' + res.body.email
                        });
                });
            })
            .catch(error => res.json({
                status: 'failed',
                message: error
            }));
    }
};