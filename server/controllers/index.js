const authController = require('./authcontroller');
const userController = require('./user_controller');
const rentalController = require('./rental_controller');
const paymentController = require('./payment_controller');
const tripsController = require('./trips_controller');
const organizerController = require('./organiser_controller');
const zoneController = require('./zone_controller');

module.exports = {
    authController,
    userController,
    rentalController,
    paymentController,
    tripsController,
    organizerController,
    zoneController

};