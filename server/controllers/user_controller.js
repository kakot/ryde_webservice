const User = require('../models').user;
const auth = require('./authcontroller').auth_register;

module.exports = {
    createUser: function (req, res) {
        return User
            .create({
                FULL_NAME: req.body.name,
                EMAIL: req.body.email,
                PHONE_NUMBER: req.body.phone,
                PASSWORD: req.body.password,
                UUID: ''
            })
            .then(user => {
                auth(user, res);

            })
            .catch(error => res.json({
                status: 'failed',
                message: error
            }));
    },
};