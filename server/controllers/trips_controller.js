const tripsModel = require('../models').trip;
const user = require('../models').user;
const zone = require('../models').zone;

module.exports = {

    allTrips: (req, res) => {
        return tripsModel.findAll({
            attributes: ['id', 'ROUTE', 'WEEKLY_PRICE', 'MONTHLY_PRICE'],
            include: [{
                attributes: ['NAME'],
                model: zone,
                as: 'zone',
            }]
        })
            .then(trips => res.send(trips))
            .catch(error => res.json({
                status: 'failed',
                message: error
            }));
    },

    userTrips: (req, res) => {

        return user.findAll({
            attributes: [],
            include: [{
                model: tripsModel,
                attributes: ['id', 'ROUTE', 'WEEKLY_PRICE', 'MONTHLY_PRICE'],
                as: 'trips',
                required: false,
                include: [{
                    model: zone,
                    attributes: ['NAME'],
                    as: 'zone',
                    required: false,
                }],
                through: { attributes: [] }
            }],
            where: {
                UUID: req.body.uuid
            }
        })
            .then(trips => res.send(trips))
            .catch(error => res.json({
                status: 'failed',
                message: error
            }));
    },

    createTrip:  (req, res) => {
        return tripsModel
            .create({
                ROUTE: req.body.route,
                WEEKLY_PRICE: req.body.weekly,
                MONTHLY_PRICE: req.body.monthly,
                MORNING: req.body.morning_time,
                EVENING: req.body.evening_time,
                ZONE: req.body.zone
            })
            .then(trip => {
                // send feedback here
                res.json({
                    status: 'success',
                    trip: trip
                })
            })
            .catch(error => res.json({
                success: false,
                message: error
            }));
    }
};