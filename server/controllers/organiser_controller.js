const organizerModel = require('../models').organizer;
const organizerCharge = require('../models').organizer_charge;

module.exports = {

    organize: (req, res) => {

        return organizerModel.create({
            NAME: req.body.name,
            PICK_UP_POINT: req.body.pick_up_point,
            DESTINATION: req.body.destination,
            CONTACT_NO: req.body.phone,
            EMAIL: req.body.email,
            NO_OF_PASSENGERS: req.body.passengers,
            PRICE_PER_SEAT: req.body.seats,
            RYDE_VEHICLE: req.body.ryde_vehicle,
            NO_OF_VEHICLES: req.body.vehicles,
            VEHICLE_TYPE: req.body.vehicle_type,
            USER: req.body.uuid
        })
            .then(organizer => {
                organizerCharge.findAll({
                    attributes: ['NO_OF_DAYS', 'CHARGE']
                })
                    .then(charges => {
                        res.json({
                            status: 'success',
                            data: charges
                        });
                    })
                    .catch(error => {
                        res.json({
                            status: 'failed',
                            data: error
                        });

                    });
            })

            .catch(error => {
                res.json({
                    status: 'failed',
                    data: error
                })
            })
    },
    
    userOrganized: (req, res) => {

        return organizerModel.findAll({
            attributes: ['NAME', 'PICK_UP_POINT', 'DESTINATION', 'CONTACT_NO', 'EMAIL', 
                'NO_OF_PASSENGERS', 'PRICE_PER_SEAT', 'RYDE_VEHICLE', 'NO_OF_VEHICLES', 
                'VEHICLE_TYPE'],
            where: {
                USER: req.body.uuid
            }
        })
            .then(organizers => {
                res.json({
                    status: 'success',
                    data: organizers
                });
            })
            .catch(error => {
                res.json({
                    status: 'failed',
                    data: error
                });
            });
    }
};
