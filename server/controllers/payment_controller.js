const merc = require('../../config');
const btoa = require('btoa');
const request = require('request');
const payment = require('../models').payment;
const user_trip = require('../models').user_trip;

module.exports = {

    allPayments: (req, res) => {
        return payment.findAll()
            .then(payments => res.send(payments))
            .catch(error => res.json({
                status: 'failed',
                message: error
            }));
    },

    userPayments: (req, res) => {
        return payment.findAll({
            where: {
                clientReference: req.body.uuid,
            }
        })
            .then(payments => res.send(payments))
            .catch(error => res.json({
                status: 'failed',
                message: error
            }));
    },

    pay: (req, res) => {

        const request_url = 'https://api.hubtel.com/v1/merchantaccount/merchants/' + merc.merchant + '/receive/mobilemoney';

        const options = {
            url: request_url,
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Basic ' + btoa(merc.client_id + ":" + merc.client_secret),
                'Cache-Control': 'no-cache'
            },
            form: {
                CustomerName: req.body.name,
                CustomerMsisdn: req.body.phone,
                CustomerEmail: req.body.email,
                Channel: req.body.channel,
                Amount: parseFloat(req.body.amount.toString()),
                PrimaryCallbackUrl: 'https://ryde-rest.herokuapp.com/confirmPayment',
                SecondaryCallbackUrl: '',
                Description: req.body.description,
                ClientReference: req.body.uuid,
            },
        };
        request(options, function (err, ress, body) {
            let json = JSON.parse(body);
            //TODO https://developers.hubtel.com/documentations/merchant-account-api#error
            if (json.ResponseCode === '0001')
                user_trip.create({
                    USER: req.body.userId,
                    TRIP: req.body.tripId
                })
                    .then(() => {
                        res.json({
                            status: 'successful',
                            message: json.Data.Description
                        });
                    })
                    .catch(error => {
                        console.log(error);
                    })

            //  res.send(json);
        });


    },

    confirmPayment: (req, res) => {

        // console.log(req);
        //TODO https://developers.hubtel.com/documentations/merchant-account-api#error
        // if payment was not complete, delete the last record of the user from the user_trip table
        payment.create({
            transactionId: req.body.Data.TransactionId,
            externalTransactionId: req.body.Data.ExternalTransactionId,
            clientReference: req.body.Data.ClientReference,
            description: req.body.Data.Description,
            amount: req.body.Data.Amount
        })
            .then(payment => {
                // send email to admin
                res.send('ok');
            })
            .catch(error => {
                // send email to admin
                console.log(error);
                res.send('error');
            })
    }

};