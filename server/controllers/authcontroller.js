const User = require('../models').user;
const key = require('../../config').app_secret;
const jwt = require('jsonwebtoken');
const hash = require('password-hash');


module.exports = {
    authenticate: function(req, res) {

        // find the user
        User.findAll({
            attributes: ['id', 'FULL_NAME', 'EMAIL', 'PHONE_NUMBER', 'UUID', 'PASSWORD'],
            limit: 1,
            where: {
                EMAIL: req.body.email
            }
        })
            .then(user => {
                if (user.length <= 0) {
                    res.json({
                        status: 'failed',
                        message: 'no user exists with this email'
                    });
                }
                else {
                    if (!hash.verify(req.body.password, user[0].PASSWORD)) {
                        res.json({success: false, message: 'Authentication failed. Wrong password.'});
                    } else {

                        jwt.sign({
                            name: user[0].FULL_NAME,
                            email: user[0].EMAIL,
                            phone: user[0].PHONE_NUMBER,
                            uuid: user[0].UUID,
                            userId: user[0].id
                        }, key, {expiresIn: '24h'}, function (err, token) {
                            if (err) res.json({
                                status: 'failed',
                                message: err
                            });
                            if (token) res.json({
                                status: 'success',
                                token: token,
                                user: user[0],
                            });
                        });
                    }
                }
        }).catch(err => {
            res.json({
                status: 'failed',
                message: err
            })
        });
    },
    auth_register: function(pUser, res) {

               jwt.sign({name: pUser.FULL_NAME,
                   email: pUser.EMAIL,
                   phone: pUser.PHONE_NUMBER,
                   uuid: pUser.UUID,
                   userId: pUser.id
               }, key, { expiresIn: '24h' }, function (err, token) {
                    if (err) res.json({
                        status: 'failed',
                        message: 'Authentication Failed'
                    });
                    if (token) res.json({
                        status: 'success',
                        token: token,
                        user: pUser
                    });
                });
    },

    verify: function (req, res, next) {

        let token = req.headers['x-access-token'];
        if (!token) return res.json({
            status: 'failed',
            message: 'no token given'
        });

        jwt.verify(token, key, function(err, decoded) {
            if (err) return res.json({
                status: 'failed',
                message: 'failed to authenticate'
            });
            if (decoded) {
                req.body.uuid = decoded.uuid;
                req.body.userId = decoded.userId;
                req.body.email = decoded.email;
                req.body.phone = decoded.phone;
                req.body.name = decoded.name;
                next();
            }
        });
    }
};