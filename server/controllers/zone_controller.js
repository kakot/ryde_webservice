const zoneModel = require('../models').zone;


module.exports = {
    zone: function (req, res) {
        return zoneModel
            .create({
                ZONE: req.body.name
            })
            .then(zone => {
                res.json({
                    status : 'success',
                    data: zone
                })
            })
            .catch(error => res.json({
                status: 'failed',
                data: error
            }));
    },
};